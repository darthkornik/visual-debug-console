# Visual Debugging console
---

## Overview:
##### Visual debugging console unity package was created in order to speed up debugging and testing processes in unity projects for programmers, testers and graphic designers. It consists of several key components.
#
## Key features:
* ### Runtime variable monitoring and manipulation
* ### Runtime remote function calling
* ### Worldspace 3D labels
###
---
# Features overview

### Runtime Variable monitoring and manipulation:

Runtime variable monitoring and manipulation enables the user to preview values of fields and properties inside an object and change without the need to access them from the inspector. Every serializable type that can be converted to string can be monitored. When it comes to manipulation it is limited by the types that can be changed, however:

* ints
* floats
* doubles
* strings
* chars

can be manipulated freely while using proper formatting.



![Variables viewed](docs/img/VariableMonitoring.PNG)

Usage of monitored variables in code with usage of workflow organization elements. Features concering visual debugger workflow organization (such as cards and categories) will be discussed further

![Code snippet](docs/img/SnippetMonitoredVariables.PNG)
![Variables viewed flow](docs/img/flow.gif)

### Remote function calling:

Remote function calling is a useful feature that enables the user to expose a button to the visual console with an C# Action connected. This way one is able to call functions on demand with just one click of the button. Apart from creating a single button, one can create a combo box that organizes functions in a clean dropdown menu. 

![Code snippet](docs/img/RegisterFunction.PNG)
![Snippet visualization](docs/img/RegisterFunctionVisualization.PNG)

Function buttons can be organized in similar way as monitored variables are. There is an additional feature of assigning colors to function buttons.

Lambda functions can be assigned to function buttons as well **NOTE: last place of function button path of a lambda function will always be its name in the console UI**

![Code snippet](docs/img/RegisteringLambdaCalls.PNG)
![Snippet visualization](docs/img/LambdaButtons.PNG)

There is also a possibility to create a **Combo box** for easier remote functions organization:

![Code snippet](docs/img/ComboBox.PNG)
![Snippet visualization](docs/img/ComboBoxViz.PNG)

Every presented here element can be organized in both a dropdown category and a isolated card. They can be nested and created as one wishes to. Categories are represented as gray buttons that expand on click and cards as green buttons that display different content on click. 

### 3D worldspace labels

3D worldspace labels make it possible to create text in world space in given points or over transforms. They look at main camera at all times and it is possible to make them follow given transforms.

![Code snippet](docs/img/Labels.PNG)


