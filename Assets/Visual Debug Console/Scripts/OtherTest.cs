using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VisualDebug;
using System.Linq;

public class OtherTest : MonoBehaviour
{
    public float variable; 

    [SerializeField] string category;
    readonly string[] categories = {"Cat1","Cat2","Cat3","Cat4"};


    private void Start()
    {
         List<int> randoms = new List<int>();
         while(randoms.Count!=3)
         {
            int random = UnityEngine.Random.Range(0,4);
            if(randoms.Contains(random))continue;
            randoms.Add(random);
            category+=categories[random];
            if(randoms.Count!=3)
            {
                category+="/";
            }
         }
        MonitoringManager.Instance.AddMonitoredVariable(this,nameof(variable),category,"MultipleObjectTest");
    }
}
