using System;
using TMPro;
using UnityEngine.EventSystems;

[Serializable]
public class ComboBox : UIBehaviour
{
    public TextMeshProUGUI Label = null;
    public TMP_Dropdown Dropdown;
}
