using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Random = System.Random;
using VisualDebug;
using UnityEngine.UI;

public class TestScript : MonoBehaviour
{
    public class TestingClass{
        int _integer;
        string _someText;
        public TestingClass(int integer, string someText)
        {
            _integer=integer;
            _someText=someText;
        }
        public override string ToString()
        {
            return $"int : {_integer} string: {_someText} ";
        }
    }

    TestingClass _testObject = new TestingClass(5,"veryveryveryveryveryveryveryverylongText");
    public float variable; 
    [SerializeField] private string sValue;
    [SerializeField] private Transform testCube = null;
    [SerializeField] private OtherTest otherObject = null;

    [field: SerializeField]
    public string s2Value
    {
        get;
        private set;
    }
    private Vector3 vec = Vector3.zero;
    private List<int> _ints = new List<int>() {1, 2, 3, 4, 5, 6};
    private Vector2 _testvec = Vector2.left;
    const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private static Random random = new Random();
    
    private bool _toggle;

    private void Start()
    {
        MonitoringManager.Instance.AddMonitoredVariable(this,nameof(variable),"","DeepCards/Card1/Card2/Card3");
        MonitoringManager.Instance.AddMonitoredVariable(this,nameof(variable),"","DeepCards/Card1/Card2/Card4");
        MonitoringManager.Instance.AddMonitoredVariable(this,nameof(variable),"","DeepCards/Card1/Card2/Card5");
        MonitoringManager.Instance.AddMonitoredVariable(this,nameof(variable),"","DeepCards/Card1/Card2/Card6");
        MonitoringManager.Instance.AddMonitoredVariable(this,nameof(variable),"","DeepCards/Card3/Card1/Card1/Card2");
        MonitoringManager.Instance.AddMonitoredVariable(this,nameof(variable),"","DeepCards/Card3/Card1/Card1/Card3");
        MonitoringManager.Instance.AddMonitoredVariable(this,nameof(variable),"","DeepCards/Card2/Card1");
        MonitoringManager.Instance.AddMonitoredVariable(this,nameof(variable),"","DeepCards/Card2/Card1/Card1/Card2");
        MonitoringManager.Instance.AddMonitoredVariable(this,nameof(variable),"","DeepCards/Card5");
        MonitoringManager.Instance.AddMonitoredVariable(this,nameof(variable),"","DeepCards/Card4/Card3");
        
        variable = Mathf.Sin(Time.deltaTime);
        // //creates a monitored variable of a variable named 'variable' in no category and main card
        MonitoringManager.Instance.AddMonitoredVariable(this,nameof(variable));
        MonitoringManager.Instance.AddMonitoredVariable(this,nameof(sValue),"category","main");       
        MonitoringManager.Instance.AddMonitoredVariable(otherObject,nameof(otherObject.variable));       
        MonitoringManager.Instance.AddMonitoredVariable(this,nameof(_ints),"collections/1/2/3","card");
        MonitoringManager.Instance.AddMonitoredVariable(this,nameof(_testObject),"ints2","card1/card2/card3");
        s2Value = "essa";
        MonitoringManager.Instance.AddMonitoredVariable(this,nameof(s2Value));       
        
        RemoteFunctionOperator.Instance.RegisterRemoteFunction(this,(b)=>Toggle(b), "", "", buttonName : "toggle").image.color = _toggle ? Color.cyan : Color.white;
        RemoteFunctionOperator.Instance.RegisterRemoteFunction(MonitoringManager.Instance,(b) => SayHi(),"");
        
        //Create a label with label ID of point zero text and value of Point 0 with a modified scale of 2
        LabelManager.Instance.CreateLabelAtPoint(Vector3.up,"point zero text","Point 0",2f);
        //Create a label over a test transform cube. Follow it. The text won't appear over other elements that are closer to the camera
        LabelManager.Instance.CreateLabelOverTransform(testCube,"CubeLabel","Cube",1f,Color.magenta,true,false);
        //Update poisition of previously created label with certain ID
        LabelManager.Instance.UpdateLabelPosition("point zero text",Vector3.up);
        //Update text of previously created label with certain ID:
        LabelManager.Instance.UpdateLabelText("point zero text",("Up vector"));

        RemoteFunctionOperator.Instance.RegisterRemoteFunction(this,(b) => SayHi(),"collections","");


        // MonitoringManager.Instance.AssignVariableToCategory(this,nameof(s2Value),"I love categories");
        
        foreach (char VARIABLE in chars)
        {
            Action sayLetter = () => { Say(VARIABLE.ToString()); };
            RemoteFunctionOperator.Instance.RegisterRemoteFunction(this,(b) => sayLetter(),"alphabet","main",new Color(UnityEngine.Random.value,UnityEngine.Random.value,UnityEngine.Random.value),VARIABLE.ToString());
        }

        Dictionary<string, Action> dict = new Dictionary<string, Action>();

        foreach (var VARIABLE in chars)
        {
            Action sayLetter = () => { Say(VARIABLE.ToString()); };
            dict.Add(VARIABLE.ToString(),sayLetter);
        }
        
        RemoteFunctionOperator.Instance.CreateComboBox(dict,"Here/Lies/Combo/Box","e","Letters");
        MonitoringManager.Instance.AddMonitoredVariable(this,nameof(variable),"1/2/3","1/2/3");
        RemoteFunctionOperator.Instance.RegisterParametrizedFunction(this,nameof(TestMethod));
        RemoteFunctionOperator.Instance.RegisterParametrizedFunction(this,nameof(TestMethod2),"","MethodWithParameter");
    }
    public void TestMethod(string a, int b, TestingClass t)
    {
    
    }
    public void TestMethod2(string a, int b)
    {
        Debug.Log($"Testing method with parameters!!! string par : {a}  int par : {b}");
    }
    void Update()
    {
        sValue = new string(Enumerable.Repeat(chars, 5).Select(s => s[random.Next(s.Length)]).ToArray());
        vec.z = random.Next();
    }

    private void Say(string word)
    {
        Debug.Log(word);
    }
    
    private void SayHi()
    {
        Debug.Log("Hi!");
    }

    private void Toggle(Button button)
    {
        _toggle = !_toggle;
        Debug.Log($"Toggle is: {_toggle}");
        button.image.color = _toggle ? Color.cyan : Color.white;
    }
}
